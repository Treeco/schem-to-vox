use dot_vox::{DotVoxData, Model, Size, Voxel};
use nbt::Value::Short;

fn main() {
    let mut path =
        std::path::PathBuf::from(std::env::args_os().nth(1).expect("Please include filename"));
    let mut file = std::fs::File::open(&path).expect("Cannot open specified file");
    let nbtstuff = nbt::Blob::from_gzip_reader(&mut file).expect("Can not read NBT data from file");

    let bytes = match nbtstuff.get("BlockData") {
        Some(nbt::Value::ByteArray(bytes)) => bytes,
        _ => panic!("Cannot find BlockData in NBT object"),
    };

    let size = match (
        nbtstuff.get("Height"),
        nbtstuff.get("Length"),
        nbtstuff.get("Width"),
    ) {
        (Some(Short(height)), Some(Short(length)), Some(Short(width))) => Size {
            x: *length as u32,
            y: *width as u32,
            z: *height as u32,
        },
        _ => panic!("Cannot find size information in NBT object"),
    };

    let voxels: Vec<Voxel> = {
        let mut voxels: Vec<Voxel> = Vec::with_capacity(bytes.len());
        let mut index = 0;
        let mut i = 0;
        while i < bytes.len() {
            let mut value = 0;
            let mut varint_length = 0;
            loop {
                let current_byte = unsafe { std::mem::transmute::<i8, u8>(bytes[i]) };
                value |= (current_byte & 127) << (varint_length * 7);
                varint_length += 1;
                if (current_byte & 128) != 128 {
                    i += 1;
                    break;
                }
                i += 1;
            }
            voxels.push(Voxel {
                x: ((index % (size.x * size.y)) / size.y) as u8,
                y: ((index % (size.x * size.y)) % size.y) as u8,
                z: (index / (size.x * size.y)) as u8,
                i: value as u8,
            });
            index += 1;
        }
        voxels
    };

    // println!("{:?}", voxels.capacity());
    // println!("{:?}", voxels.len());
    // println!("{:?}", size);
    // println!("{:?}", voxels);
    // println!("{:?}", path);

    let testmodel = Model { size, voxels };
    let testvox = DotVoxData {
        version: 150,
        models: vec![testmodel],
        palette: dot_vox::DEFAULT_PALETTE.clone(),
        materials: Vec::new(),
    };
    path.set_extension("vox");
    let mut file = std::fs::File::create(&path).expect("Could not create file");
    testvox
        .write_vox(&mut file)
        .expect("Could not write to vox");
}
