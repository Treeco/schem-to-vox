A converter for modern Minecraft schematic files (`.schem`), to MagicaVoxel files (`.vox`), written in Rust

## Usage

`cargo run filename.schem`, or `./schem-to-vox filename.schem`
A vox file will be created with the same filename as the input.

Palettes are not currently supported, each Minecraft BlockState simply gets assigned an index in MagicaVoxel's default palette.
